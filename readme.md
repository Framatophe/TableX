# Utilisation

   * Sous Linux : Se placer dans le Shell
   * Se placer dans le dossier contenant le logiciel Table X
   * Lancer en ligne de commande: $python3 TableX.py

Bon jeu

## 04 JUIN 2007

Le logiciel TableX est né de la collaboration de Stéphane PILLOT et de Loïc CHARDONNET.

## 19 MARS 2015

Modifications mineures (agencement fenêtre, couleurs...) par Christophe MASUTTI, Quentin THEURET

Le logiciel est sous licence libre CECILL et il est possible de le modifier à partir du moment ou les modifications apportées soient explicitées dans la nouvelle version et que les développeurs originels soient nommés.