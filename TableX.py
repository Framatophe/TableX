#!/usr/bin/python3.4
# -*- coding: utf-8 -*-
# Python 3

from tkinter import *
from random import randrange

BG_COLOR = '#C5EFF7'
BUTTON_COLOR = '#3498DB'

def dem_pseudo():
    "Demande le pseudo de l'utilisateur"
    global texte1, bouton1, entree1, compteur1, compteur2
    texte1 = Label(fenetre1, text='Entre ton prénom', font="Sans 15", bg=BG_COLOR)
    texte1.pack()
    entree1 = Entry(fenetre1)
    entree1.pack()
    entree1.focus_set()
    bouton1 = Button(fenetre1, text='OK', command=(lambda:choix_pseudo(0)), bd="5", bg=BUTTON_COLOR, relief="flat")
    bouton1.pack()
    bouton2 = Button(fenetre1, text='Quitter le jeu', command=fenetre1.destroy, width=20, bd="5", bg=BUTTON_COLOR, relief="flat")
    bouton2.pack(side=BOTTOM)
    rajout=Label(fenetre1,text="|-- But du jeu --| \n Choisi la table de multiplication \n et trouve les bons résultats",fg='black', bg=BG_COLOR, font="Sans 10")
    rajout.pack(side=BOTTOM)
    fenetre1.bind('<Return>', choix_pseudo)
    fenetre1.bind('<KP_Enter>', choix_pseudo)

def choix_pseudo(event):
    "Choix du pseudo"
    global texte1, bouton1, entree1, compteur1, compteur2, pseudo
    try:
        str(entree1.get())
    except:
        pseudo = "anonyme"
    else:
        pseudo = str(entree1.get())
        if pseudo == "":
            pseudo = "anonyme"
    entree1.destroy()
    bouton1.destroy()
    dem_table()
    

def dem_table():
    "Demande à l'utilisateur la table"
    global texte1, bouton1, entree1, compteur1, compteur2, pseudo
    texte1.configure(text='Quelle table de multiplication veux-tu réviser ?')
    entree1 = Entry(fenetre1)
    entree1.pack()
    entree1.focus_set()
    bouton1 = Button(fenetre1, text='Valider', command=(lambda:choix_table(0)), bd="5", bg=BUTTON_COLOR, relief="flat")
    bouton1.pack()
    fenetre1.bind('<Return>', choix_table)
    fenetre1.bind('<KP_Enter>', choix_table)
    

def choix_table(event):
    "Choix de la table à réviser"
    global table, compteur1, compteur2, texte1, bouton1, entree1, pseudo
    try:
        int(entree1.get())
    except:
        table = randrange(1,10)
    else:
        table = int(entree1.get())
    entree1.destroy()
    bouton1.destroy()
    choix_chiffre()

def choix_chiffre():
    "Choix du chiffre"
    global table, chiffre, entree1, bouton1, texte1, compteur1, compteur2, pseudo
    chiffre = randrange(1,10)
    texte1.configure(text=str(chiffre)+' x '+str(table)+' =')
    entree1 = Entry(fenetre1)
    entree1.pack()
    entree1.focus_set()
    compteur1 += 1
    bouton1 = Button(fenetre1, text='Valider', command=(lambda:val_mult(0)), bd="5", bg=BUTTON_COLOR, relief="flat")
    bouton1.pack()
    fenetre1.bind('<Return>', val_mult)
    fenetre1.bind('<KP_Enter>', val_mult)
    
def val_mult(event):
    "Validation de la multiplication"
    global table, chiffre, entree1, bouton1, compteur2, bouton2, texte1, texte2, texte3, compteur1, compteur2, pseudo
    bouton1.destroy()
    res_juste = table * chiffre
    try:
        int(entree1.get())
    except:
        res_utilisateur = 0
    else:
        res_utilisateur = int(entree1.get())

    entree1.destroy()
    if res_juste == res_utilisateur:
        texte1.configure(text='Bravo '+str(pseudo)+' ! Tu as trouvé !')
        compteur2 += 1
    else:
        texte1.configure(text='Faux, la réponse était : '+str(res_juste))
    if compteur1 == 10:
        taux = compteur2 * 100 / 10
        texte2 = Label(fenetre1, text='Ton taux de réussite est de : '+str(taux)+' %', font="Sans 15", bg=BG_COLOR, fg="#cc2d18")
        texte2.pack()
        texte3 = Label(fenetre1, text='Veux tu recommencer ?', font="Sans 15", bg=BG_COLOR)
        texte3.pack()
        bouton1 = Button(fenetre1, text='«« oui', command=(lambda:recommencer(0)), bd="5", bg=BUTTON_COLOR, relief="flat")
        bouton1.pack(side=LEFT)
        bouton2 = Button(fenetre1, text='non »»', command=fenetre1.destroy, bd="5", bg=BUTTON_COLOR, relief="flat")
        bouton2.pack(side=RIGHT)
        fenetre1.bind('<Return>', recommencer)
        fenetre1.bind('<KP_Enter>', recommencer)
    else:
        bouton1 = Button(fenetre1, text='on continue :)', command=(lambda:continuer(0)), bd="5", bg=BG_COLOR, relief="flat")
        bouton1.pack()
        fenetre1.bind('<Return>', continuer)
        fenetre1.bind('<KP_Enter>', continuer)

def recommencer(event):
    "Réinitialisateur"
    global table, chiffre, entree1, bouton1, compteur2, bouton2, texte1, texte2, texte3, compteur1, compteur2, pseudo
    texte2.destroy()
    texte3.destroy()
    bouton1.destroy()
    bouton2.destroy()
    compteur1 = 0
    compteur2 = 0
    dem_table()

def continuer(event):
    "Réinitialisateur partiel"
    global table, chiffre, entree1, bouton1, compteur2, bouton2, texte1, texte2, texte3, compteur1, compteur2, pseudo
    bouton1.destroy()
    choix_chiffre()

# Programme principal
fenetre1 = Tk()
fenetre1.title('TableX.-V4.2')
fenetre1.geometry('500x300')
fenetre1.configure(bg=BG_COLOR)
logo=PhotoImage(file="logo.gif")
labl = Label(fenetre1, image=logo)
labl.pack(side=TOP)
compteur1 = 0
compteur2 = 0
dem_pseudo()
fenetre1.mainloop()
